package dev.rehm.controllers;

import dev.rehm.models.Item;
import dev.rehm.services.ItemService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

    /*
    GET /items
    POST /items
    GET /items/id
    PUT /items/id
    PATCH /items/id
    DELETE /items/id

    GET /items
    GET /items?name=str&min-price=0&max-price=100
    GET /items?min-price=0&max-price=100
    GET /items?name=str&max-price=100
     */

@RestController // (1) registering this class as a spring bean (2) returns a response body from each method
// @RestController is equivalent to @Controller + @ResponseBody for each handler method
@RequestMapping("/items") //prepends this to each request URI
@CrossOrigin // helps out with cors issues
public class ItemController {

    private static Logger logger = LogManager.getLogger(ItemController.class);

    @Autowired
    private ItemService itemService;

    @GetMapping
    public ResponseEntity<List<Item>> getAllItems(@RequestParam(value = "name",required = false)String nameParam,
                                  @RequestParam(value = "min-price", required = false)Double minPrice,
                                  @RequestParam(value = "max-price", required = false)Double maxPrice){
        if(nameParam!=null){
            logger.info("Getting items by name: {}", nameParam);
            return ResponseEntity.ok().body(itemService.getByName(nameParam));
        }
        logger.info("Getting all items");
        return ResponseEntity.ok().body(itemService.getAll());
    }

    @GetMapping(value = "/{id}",produces = "application/json")
    public ResponseEntity<Item> getItemById(@PathVariable("id") int id){
        logger.info("Getting item with id: {}", id);
        return ResponseEntity.ok().body(itemService.getById(id));
    }

//    @ResponseStatus(HttpStatus.CREATED) // the ResponseStatus annotation can be used to send an HTTP status code
//    from a method, it can also be associated with custom exceptions if you want to throw an exception and return a
//    corresponding status code
    @PostMapping(consumes = "application/json")
    public ResponseEntity<Item> createNewItem(@RequestBody Item item){
        logger.info("creating a new item: {}",item);
        itemService.create(item);
        return new ResponseEntity<>(item,HttpStatus.CREATED);
    }




}
