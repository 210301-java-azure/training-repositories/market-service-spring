package dev.rehm.repositories;

import dev.rehm.models.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository extends JpaRepository<Item, Integer> { // this is our dao interface

    public List<Item> findItemsByNameContaining(String someVariable);

}
